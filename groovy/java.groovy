import hudson.model.JDK
import hudson.tools.InstallSourceProperty
import hudson.tools.ZipExtractionInstaller
def descriptor = new JDK.DescriptorImpl();
def List<JDK> installations = []
javaTools=[['name':'jdk8', 'url':'file:/var/jenkins_home/downloads/openjdk-8u40-b25-linux-x64-10_feb_2015.tar.gz', 'subdir':'java-se-8u40-ri'],
      ['name':'jdk8', 'url':'file:/var/jenkins_home/downloads/openjdk-8u40-b25-linux-x64-10_feb_2015.tar.gz', 'subdir':'java-se-8u40-ri']]
javaTools.each { javaTool ->
    println("Setting up tool: ${javaTool.name}")
    def installer = new ZipExtractionInstaller(javaTool.label as String, javaTool.url as String, javaTool.subdir as String);
    def jdk = new JDK(javaTool.name as String, null, [new InstallSourceProperty([installer])])
    installations.add(jdk)
}
descriptor.setInstallations(installations.toArray(new JDK[installations.size()]))
descriptor.save()
